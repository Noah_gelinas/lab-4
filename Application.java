import java.util.Scanner;
public class Application{
	public static void main(String... args) {
		Scanner sc=new Scanner(System.in);
		int amountStudied=sc.nextInt();
		
		Student youran=new Student(93,"youran");			
		System.out.println(youran.getGrade());
		System.out.println(youran.getName());
		System.out.println(youran.getYear());
		System.out.println(youran.getAmountLearnt());
		
		
		Student Tommy=new Student(89,"Tommy");
	
		System.out.println(Tommy.getGrade());
		System.out.println(Tommy.getName());
		System.out.println(Tommy.getYear());
		
	
		Student[] section3=new Student[3];
		section3[0]=youran;
		
		section3[1]=Tommy;
		
		section3[2]=new Student(85,"Noah");		
		section3[2].learn(amountStudied);
		section3[2].learn(amountStudied);
		
		section3[1].learn(5);	
		section3[0].learn(-5);
		
		System.out.println(section3[0].getGrade());
		System.out.println(section3[2].getGrade());
		
		System.out.println(section3[0].getAmountLearnt());
		System.out.println(section3[1].getAmountLearnt());
		System.out.println(section3[2].getAmountLearnt());
		
		System.out.println(section3[0].getAmountLearnt());
		System.out.println(section3[1].getAmountLearnt());
		System.out.println(section3[2].getAmountLearnt());
		
		Student jon=new Student(85,"Jon");
		jon.setYear(2020);
		System.out.println(jon.getAmountLearnt());
		System.out.println(jon.getGrade());
		System.out.println(jon.getName());
		System.out.println(jon.getYear());
	}
}